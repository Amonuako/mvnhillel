package org.amonuako;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class ArrayManipulationTest {
    private final ArrayManipulation arrayManipulator = new ArrayManipulation();

    @Test
    public void testExtractValuesAfterLastFour() {
        int[] inputArray = {1, 2, 4, 4, 2, 3, 4, 1, 7};
        int[] expectedOutput = {1, 7};
        assertArrayEquals(expectedOutput, arrayManipulator.addArray(inputArray));
    }

    @Test
    public void testContainsOnesAndFours() {
        int[] inputArray1 = {1, 1, 1, 4, 4, 1, 4, 4};
        assertTrue(arrayManipulator.containsOnesAndFours(inputArray1));

        int[] inputArray2 = {1, 1, 1, 1, 1, 1};
        assertFalse(arrayManipulator.containsOnesAndFours(inputArray2));

        int[] inputArray3 = {4, 4, 4, 4};
        assertFalse(arrayManipulator.containsOnesAndFours(inputArray3));

        int[] inputArray4 = {1, 4, 4, 1, 1, 4, 3};
        assertFalse(arrayManipulator.containsOnesAndFours(inputArray4));
    }
}
