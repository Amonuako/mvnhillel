package org.amonuako;

import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

public class ResultOutputAndWrite {
    private String packagePath;

    public ResultOutputAndWrite(String packagePath) {
        this.packagePath = packagePath;
    }

    public void printAndWriteResults() {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(DiscoverySelectors.selectPackage(packagePath))
                .build();
        Launcher launcher = LauncherFactory.create();
        SummaryGeneratingListener listener = new SummaryGeneratingListener();

        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);

        TestExecutionSummary summary = listener.getSummary();
        long time = summary.getTimeFinished() - summary.getTimeStarted();

        System.out.println("Кількість знайдених тестів " + summary.getTestsFoundCount());
        System.out.println("Кількість успішно пройдених тестів " + summary.getTestsSucceededCount());
        System.out.println("Кількість неуспішних тестів " + summary.getTestsFailedCount());
        System.out.println("Часу пройшло: " + time + " мс");

        try {
            File file = new File("results.txt");
            FileWriter fr = new FileWriter(file, true);
            BufferedWriter br = new BufferedWriter(fr);
            br.write("Кількість знайдених тестів " + summary.getTestsFoundCount() + "\n");
            br.write("Кількість успішно пройдених тестів " + summary.getTestsSucceededCount() + "\n");
            br.write("Кількість неуспішних тестів " + summary.getTestsFailedCount() + "\n");
            br.write("Часу пройшло: " + time + " мс");
            br.close();
            fr.close();
            System.out.println("Результати записано у файл.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    }


