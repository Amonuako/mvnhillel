package org.amonuako;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMathLibraryTest {

    @Test
    public void testAdd() {
        SimpleMathLibrary mathLibrary = new SimpleMathLibrary();
        double result = mathLibrary.add(3, 3);
        if (result == 5) {
            System.out.println("OK");
        } else {
            System.out.println("NOK");
        }
        assertEquals(5, result, 0);
    }

    @Test
    public void testMinus() {
        SimpleMathLibrary mathLibrary = new SimpleMathLibrary();
        double result = mathLibrary.minus(5, 3);
        if (result == 2) {
            System.out.println("OK");
        } else {
            System.out.println("NOK");
        }
        assertEquals(2, result, 0);
    }
}
