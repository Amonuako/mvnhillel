package hw28;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DataBaseConnection {

    private static Connection connection ;
    private static final String login;
    private static final String password;
    private static final String url ;
    private static final String databaseName ;

    private static final Properties PROPERTIES = new Properties();

    static {
        try {
            PROPERTIES.load(new FileInputStream(new File("src/main/resources/application.properties")));
            url = PROPERTIES.getProperty("url");
            login = PROPERTIES.getProperty("login");
            password = PROPERTIES.getProperty("password");
            databaseName = PROPERTIES.getProperty("databaseName");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DataBaseConnection() {}

    public  static synchronized Connection getConnection() throws SQLException, IOException {
        if (connection == null|| connection.isClosed()) {
            connection = DriverManager.getConnection(url+ "/" + databaseName, login, password);
        }
        return connection;
    }

    public static void close() throws SQLException {
       try { if (connection != null) {
            connection.close();
            System.out.println("Connection closed.");
        }
           } catch (SQLException e) {
        System.out.println("Error closing connection: " + e.getMessage());
        }

}
}
