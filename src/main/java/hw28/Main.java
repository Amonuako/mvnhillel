package hw28;

import java.io.IOException;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, IOException {

       LessonDao lessonDao = new LessonDao();
       Homework homework = new Homework(3,"HillelHw3","Descr3");
       lessonDao.addHomework(homework);
       lessonDao.addLesson(new Lesson(3,"Hillel3",homework));
//     lessonDao.deleteHomework(1);
       lessonDao.getAllLessons();
//     lessonDao.deleteLesson(3);
//     lessonDao.getAllHomeworks();

    }
}
