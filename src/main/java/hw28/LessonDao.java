package hw28;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LessonDao {
    private static final String INSERT_LESSON =
            "INSERT INTO Lesson (id, name, homework_id) VALUES (?, ?, ?)";
    private static final String DELETE_LESSON =
            "DELETE FROM Lesson WHERE id = ?";
    private static final String SELECT_ALL_LESSONS =
            "SELECT l.id, l.name, h.id, h.name, h.description FROM Lesson l LEFT JOIN Homework h ON l.homework_id = h.id";
    private static final String SELECT_LESSON_BY_ID =
            "SELECT l.id, l.name, h.id, h.name, h.description FROM Lesson l JOIN Homework h ON l.homework_id = h.id WHERE l.id = ?";
    private static final String INSERT_HOMEWORK =
            "INSERT INTO Homework (id, name, description) VALUES (?, ?, ?)";
    private static final String DELETE_HOMEWORK =
            "DELETE FROM Homework WHERE id = ?";
    private static final String SELECT_ALL_HOMEWORKS =
            "SELECT * FROM Homework;";

    public void addLesson(Lesson lesson) {
        try (Connection connection = DataBaseConnection.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_LESSON)) {
            preparedStatement.setInt(1, lesson.getId());
            preparedStatement.setString(2, lesson.getName());
            preparedStatement.setInt(3, lesson.getHomework().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }

    }


    public void deleteLesson(int lessonId) {
        try (Connection connection = DataBaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_LESSON)) {
            preparedStatement.setInt(1, lessonId);
            preparedStatement.executeUpdate();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
    public int addHomework(Homework homework) {
            try (Connection connection = DataBaseConnection.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HOMEWORK)) {
                preparedStatement.setInt(1, homework.getId());
                preparedStatement.setString(2, homework.getName());
                preparedStatement.setString(3, homework.getDescription());
                return preparedStatement.executeUpdate();
            } catch (SQLException | IOException e) {
                e.printStackTrace();
                return 0;
            }
        }

    public void deleteHomework(int homeworkId) {
        try (Connection connection = DataBaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_HOMEWORK)) {
            preparedStatement.setInt(1, homeworkId);
            preparedStatement.executeUpdate();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }
    public List<Homework> getAllHomeworks() {
        List<Homework> homeworks = new ArrayList<>();
        try (Connection connection = DataBaseConnection.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_ALL_HOMEWORKS)) {
            while (resultSet.next()) {
                Homework homework = new Homework(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
                homeworks.add(homework);
                System.out.println(homework.toString());
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return homeworks;
    }

    public List<Lesson> getAllLessons() {
        List<Lesson> lessons = new ArrayList<>();
        try (Connection connection = DataBaseConnection.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_ALL_LESSONS)) {
            while (resultSet.next()) {
                Homework homework = new Homework(resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5));
                Lesson lesson = new Lesson(resultSet.getInt(1), resultSet.getString(2), homework);
                lessons.add(lesson);
                System.out.println(lesson.toString());
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    public Lesson getLessonById(int lessonId) {
        try (Connection connection = DataBaseConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_LESSON_BY_ID)) {
            preparedStatement.setInt(1, lessonId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    Homework homework = new Homework(resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5));
                    Lesson lesson = new Lesson(resultSet.getInt(1), resultSet.getString(2), homework);
                    System.out.println("Found Lesson: " + lesson);
                    return lesson;
                }else
                    System.out.println("Lesson not found");
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
