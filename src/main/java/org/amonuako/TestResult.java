package org.amonuako;

public class TestResult {
    private int totalTestsFound;
    private int totalTestsSucceeded;
    private int totalTestsFailed;
    private long executionTime;
    public void printResults() {
        if(totalTestsFound==0){
            System.out.println("Не зайдено жодного тесту");
        }
        else {
        System.out.println("Загальна кількість тестів: " + totalTestsFound);
        System.out.println("Кількість успішно пройдених тестів: " + totalTestsSucceeded);
        System.out.println("Кількість провалених тестів: " + totalTestsFailed);
        System.out.println("Часу пройшло: " + executionTime +" мс");
    }
    }

    public int getTotalTestsFound() {
        return totalTestsFound;
    }

    public void setTotalTestsFound(int totalTestsFound) {
        this.totalTestsFound = totalTestsFound;
    }

    public int getTotalTestsSucceeded() {
        return totalTestsSucceeded;
    }

    public void setTotalTestsSucceeded(int totalTestsSucceeded) {
        this.totalTestsSucceeded = totalTestsSucceeded;
    }

    public int getTotalTestsFailed() {
        return totalTestsFailed;
    }

    public void setTotalTestsFailed(int totalTestsFailed) {
        this.totalTestsFailed = totalTestsFailed;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }
}
