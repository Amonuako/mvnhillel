package org.amonuako;

import java.util.Arrays;

public class ArrayManipulation {
    public int[] addArray(int[] array) {
        int lastFourIndex = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 4) {
                lastFourIndex = i;
            }
        }
        if (lastFourIndex == -1) {
            throw new RuntimeException("Масив не містить четвірок.");
        }
        return Arrays.copyOfRange(array, lastFourIndex + 1, array.length);
    }

    public boolean containsOnesAndFours(int[] array) {
        boolean containsOne = false;
        boolean containsFour = false;

        for (int value : array) {
            if (value == 1) {
                containsOne = true;
            } else if (value == 4) {
                containsFour = true;
            }
        }
        return containsOne && containsFour;
    }
}
