package org.amonuako;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;


public class TestResultParser {

    public org.amonuako.TestResult parse(String filePath) {
        TestResult testResult = new TestResult();
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("Кількість знайдених тестів")) {
                    String[] parts = line.split("\\s+");
                    if (parts.length >= 4) {
                        testResult.setTotalTestsFound(Integer.parseInt(parts[3]));
                    }
                } else if (line.startsWith("Кількість успішно пройдених тестів")) {
                    String[] parts = line.split("\\s+");
                    if (parts.length >= 5) {
                        testResult.setTotalTestsSucceeded(Integer.parseInt(parts[4]));
                    }
                } else if (line.startsWith("Кількість неуспішних тестів")) {
                    String[] parts = line.split("\\s+");
                    if (parts.length >= 4) {
                        testResult.setTotalTestsFailed(Integer.parseInt(parts[3]));
                    }
                } else if (line.startsWith("Часу пройшло")) {
                    String[] parts = line.split("\\s+");
                    if (parts.length >= 4) {
                        String timeStr = parts[3].replaceAll("[^0-9]", "");
                        if (!timeStr.isEmpty()) {
                            testResult.setExecutionTime(Long.parseLong(timeStr));
                        }
                    }
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return testResult;
    }


    public TestResult parse(Path path) {
        return parse(path.toString());
    }
}

