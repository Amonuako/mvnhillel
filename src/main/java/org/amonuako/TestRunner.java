package org.amonuako;


import org.junit.platform.engine.DiscoverySelector;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;



public class TestRunner {
    public void runTestByClassName(String className) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectClass(className))
                .build();

        executeTests(request);
    }

    public void runTestByClass(Class<?> clazz) {
        runTestByClassName(clazz.getName());
    }

    public void runTestsByClassNames(String... classNames) {
        for (String className : classNames) {
            runTestByClassName(className);
        }
    }

    public void runTestsByClasses(Class<?>... classes) {
        for (Class<?> clazz : classes) {
            runTestByClassName(clazz.getName());
        }
    }

    public void runTestsByPackageName(String packageName) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(selectPackage(packageName))
                .build();

        executeTests(request);
    }

    private void executeTests(LauncherDiscoveryRequest request) {
        Launcher launcher = LauncherFactory.create();
        SummaryGeneratingListener listener = new SummaryGeneratingListener();
        launcher.registerTestExecutionListeners(listener);
        launcher.execute(request);
        TestExecutionSummary summary = listener.getSummary();
        System.out.println(summary.toString());
    }

    private DiscoverySelector selectPackage(String packageName) {
        return DiscoverySelectors.selectPackage(packageName);
    }

    private DiscoverySelector selectClass(String className) {
        return DiscoverySelectors.selectClass(className);
    }
}
