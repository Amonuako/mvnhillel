package hw30;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(TestConfiguration.class);
        MyBean bean1 = applicationContext.getBean("bean1", MyBean.class);
        MyBean bean2 = applicationContext.getBean("bean2", MyBean.class);
        SomeClass bean3 = applicationContext.getBean("bean3",SomeClass.class);

        System.out.println(bean1.getName());
        System.out.println(bean2.getName());
        System.out.println(bean3.getName());
    }
}
