package hw30;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {
    @Bean
      public MyBean bean1(){

       return new MyBean("Bean1");
     }
     @Bean
       public MyBean bean2(){

        return new MyBean("Bean2");
     }

     @Bean
     public SomeClass bean3(){
        return new  SomeClass("anotherTypeBean");
     }
}
